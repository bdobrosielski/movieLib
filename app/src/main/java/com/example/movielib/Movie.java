package com.example.movielib;

public class Movie {
    private String name;
    private String author;
    private int pages;
    private String imageUrl;
    private String description;

    public Movie(String name, String author, int pages, String imageUrl, String description) {
        this.name = name;
        this.author = author;
        this.pages = pages;
        this.imageUrl = imageUrl;
        this.description = description;
    }
}
